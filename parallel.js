/*
To run just install the request package using npm i -g request
and node parallel.js
*/


var request = require('request');
var fs      = require('fs');

const workQueue = [
	"https://medium.com/",
	"https://medium.com/search",
	"https://medium.com/topic/culture",
	"https://medium.com/topic/technology",
	"https://medium.com/topic/startup"
];
var visited_url = [];
var url_queue   = [];


const Worker = (name) => (channel) => {
    const history = [];
    const next = () => {
        const url = channel.getWork();
        if (!url) { // All done!
            console.log('Worker ' + name + ' completed');
            return;
        }
        history.push(url);
        console.log('Worker ' + name + ' grabbed new job: ' + url + '. History is:', history);

        // Checking if URL does not exists in visited url 
        if (visited_url.indexOf(url) === -1) {
            
            request(url, function (error, response, body) {
                visited_url.push(url);
                if (!error) {
                    // Fetching HREF from the body of the page
                    page_urls = body.match(/href="(.*?)"/g);
                    // Checking urls contains elements else go to the next URL
                    if (page_urls) {
                        to_write = []
                        page_urls.forEach(page_url => {
                            // Getting the URLs
                            page_url = page_url.match(/"(.*?)"/)[1].trim();
                            to_write.push(page_url)
                            if (url_queue.indexOf(page_url) === -1) {
                                url_queue.push(page_url);
                            }
                        });
                        fs.appendFile("urls_list.txt", '\r\n\r\n' + 'Visiting URL ---> ' + url + '\r\n\r\n' + to_write.join('\r\n') + '\n\n\n', err => {
                            if (err) throw err;
                            channel.putWork(url_queue);
                            next();
                        });
                    }
                }
            });
        } else {
            next();
        }
    };
    next();
}

const Channel = (queue) => {
    return {
        getWork: () => {
            return queue.shift();
        },
        putWork: (w) => {
            // Adding the URLs to the queue
            queue = [...queue, ...w]
        }

    };
};

let channel = Channel(workQueue);
let a = Worker('a')(channel);
let b = Worker('b')(channel);
let c = Worker('c')(channel);
let d = Worker('d')(channel);
let e = Worker('e')(channel);